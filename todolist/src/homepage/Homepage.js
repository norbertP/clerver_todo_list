import React from 'react';
import NavBar from '../components/NavBar'
import { getConnectedUser } from '../auth/services'
import { getToDoListForUser, addNewTask, setTaskDone } from '../database/services'
import Task from '../components/Task'

const MAX_NBALLOWED_TASKS = 10
export class HomePage extends React.Component {
    constructor(props) {
        super(props)
        this.connectedUser = getConnectedUser()
        this.state = {
            taskList: [],
            flashMessage: undefined,
            flashMessageType: "success"
        }
        this.requestCreationNewTask = this.requestCreationNewTask.bind(this)
        this.createNewTask = this.createNewTask.bind(this)
        this.cancelNewTask = this.cancelNewTask.bind(this)
        this.refreshTaskList = this.refreshTaskList.bind(this)
        this.displayFlashMessage = this.displayFlashMessage.bind(this)
        this.setTaskAction = this.setTaskAction.bind(this)
    }


    /**
     * Affiche un message pendant quelques secondes
     * @param {*} flashMessage 
     * @param {*} flashMessageType 
     * @param {*} time 
     */
    displayFlashMessage(flashMessage, flashMessageType = "success", time = 3000) {
        this.setState({
            flashMessage,
            flashMessageType
        }, () => setTimeout(() => this.setState({ flashMessage: undefined }), time))
    }


    requestCreationNewTask = () => {
        //Test du max de message possible
        if (this.state.taskList.length >= MAX_NBALLOWED_TASKS) {
            this.displayFlashMessage(`Vous ne pouvez pas créer plus de ${MAX_NBALLOWED_TASKS} tâches.`, 'danger')
            return false
        }

        const newTask = { id: Date.now(), subject: "", dateStart: new Date(Date.now()).toLocaleTimeString(), status: "creation" }
        this.setState({ taskList: [newTask, ...this.state.taskList] })
    }

    createNewTask(task) {
        //Si la tache n'a pas de sujet on ne l'enregistre pas.
        if (task.subject.trim() === '') {
            this.displayFlashMessage(`Veuillez décrire la tâche à effectuer.`, 'danger')
            return false
        }

        //Ajout de la tache en DB pour l'utilisateur connecté.
        addNewTask(this.connectedUser.login, task)
            .then(this.refreshTaskList())
    }

    //La dernière tache créée se trouve au début du tableau
    cancelNewTask(task) {
        const [head, ...tail] = this.state.taskList
        this.setState({ taskList: tail })
    }

    refreshTaskList() {
        const todoList = getToDoListForUser(this.connectedUser)
        const taskList = todoList.tasks.filter(task => task.status === 'todo')
        this.setState({ taskList })
    }


    setTaskAction(task, action) {
        if (action == 'done')
            setTaskDone(this.connectedUser.login, task)
                .then(this.refreshTaskList())
    }

    isPendingCreation() {
        const res = (this.state.taskList.filter(task => task.status === 'creation').length > 0)
        return res
    }

    componentDidMount() {
        this.refreshTaskList()
    }

    render() {
        const taskList = this.state.taskList
        const isPendingCreation = this.isPendingCreation()
        //Si on est en création on retire l'élément en cours du comptage.
        const taskListCount = taskList.length - (isPendingCreation ? 1 : 0)

        return <div className="container">
            <NavBar />
            <div className="d-flex justify-content-between" style={{ marginTop: '40px' }}>
                {taskListCount > 0 &&
                    <h5>Vous avez <span class="text-primary">{taskListCount}</span> {taskListCount > 1 ? 'tâches' : 'tâche'} en cours.</h5>
                }
                {!taskListCount &&
                    <h5>Vous n'avez aucune tâche en cours.</h5>
                }
                {!isPendingCreation &&
                    <div>
                        <button className="btn btn-primary" onClick={this.requestCreationNewTask}>Créer une tâche</button>
                    </div>
                }
            </div>

            {this.state.flashMessage &&
                <div class={`alert alert-${this.state.flashMessageType}`} role="alert">
                    {this.state.flashMessage}
                </div>
            }
            <ul class="list-group list-group-flush" style={{ marginTop: '20px' }}>
                {taskList.map(task =>
                    <li key={task.id} className="list-group-item list-group-item-action">
                        <Task task={task} mode='read' createNewTask={this.createNewTask} cancelNewTask={this.cancelNewTask} setTaskAction={this.setTaskAction} />
                    </li>)}
            </ul>



        </div>
    }
}


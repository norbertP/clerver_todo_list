import React from 'react'

export default class Task extends React.Component {

    constructor(props)
    {
        super(props)
        this.state={
            task : this.props.task, 
        }
    }

    handleOnChange = (e) => {
        this.setState({task:{...this.state.task, subject:e.target.value }})
    }

    handleOnDone = (e) => {
        e.preventDefault()
        this.props.setTaskAction(this.state.task, "done")
    }

    handleOnCreate = (e) => {
        e.preventDefault()
        this.props.createNewTask(this.state.task)
    }

    handleOnCancel = (e) => {
        e.preventDefault()
        this.props.cancelNewTask(this.state.task)
    }
    render() {
        const task = this.state.task
        const status = task.status
        
        return <div>
            {status !== 'creation' && 
            <div className="d-flex justify-content-between">
            <span>{this.state.task.subject}</span>
            <button className="btn btn-success" onClick={this.handleOnDone}>
                <i class="fa fa-check" aria-hidden="true"></i>
            </button>
            
            </div>
            }
            {status === 'creation' &&
                <form className="form-inline d-flex justify-content-between">
                    <input className="form-control text-danger p-2 flex-fill mr-3" type="text" placeholder="Description de la tâche" aria-label="Description de la tâche" onChange={this.handleOnChange}/>
                    <div className="" >
                    <button className="btn btn-danger" onClick={this.handleOnCancel} >Annuler</button>
                    <button className="btn btn-primary ml-2" onClick={this.handleOnCreate}>Créer</button>
                    </div>
                </form>
            }
        </div>
    }
}
import React from 'react'
import {getConnectedUser} from '../auth/services' 


export default class NavBar extends React.Component {
    render() {
        const connectedUser = getConnectedUser()
        return <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a className="navbar-brand" href="#">{`Liste des choses à faire de ${connectedUser.getDisplayName()}`}</a>
                <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Se déconnecter <span className="sr-only">(current)</span></a>
                    </li>

                </ul>

            </div>
        </nav>
    }
}
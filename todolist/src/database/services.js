/**
 * Renvoi la liste des todo pour un utilisateur donnée.
 * Todo : devra être connecté à MongoDb
 * Pour le moment on va enregistrer les todo list dans un object JS avec le login de la personne dans userId.
 * @param {string} userId 
 */


var taskList = [
    {
        userId: '123',
        login:'donald-t',
        tasks: [
            { id: 10, subject: "Acheter le pain", dateStart: "17/06/2020", status: "todo" },
            { id: 20, subject: "Aller en chine", date: "19/06/2020", status: "todo" },
            { id: 30, subject: "Vendre la maison", dateStart: "01/01/2020", dateEnd: "01/01/2021", status: "todo" },
            { id: 50, subject: "Appeler Manu", dateStart: "10/06/2020", status: "done" },
        ]
    },
    {
        userId: '234',
        login: 'manu',
        tasks: [
            { id: 2, subject: "Appeler Donald", dateStart: "09/06/2020", status: "done" },
            { id: 10, subject: "Aller au marché", dateStart: "11/06/2020", status: "todo" },
            { id: 20, subject: "Aller au touquet", date: "19/06/2020", status: "cancelled" },
        ]
    }]


//Pour la démo, on s'attend à recevoir soit le login ou l'userId en paramètre, tous deux sont uniques.
export function getToDoListForUser(param) {
    const {userId, login} = param

    if (!userId && !login)
        return []

    //On va rechercher par login
    const foundList = taskList.filter(tasklist => tasklist.login === login)
    if (!foundList || foundList.length === 0)
        return []
    
    //Autrement on retourne la première liste trouvée
    return foundList[0]

}

//Todo : mettre en DB
export function addNewTask(login, task){
    task.status = "todo"
    const todoList = getToDoListForUser({login})
    let newTasks = todoList.tasks
    //newTasks.push(task)
    newTasks = [task, ...newTasks]
    const newTodoList = {...todoList, tasks:newTasks} //Ajout de la nouvelle tache
    taskList = taskList.map(list => list.login === login ? newTodoList : list)
    return Promise.resolve(taskList)
}

export function setTaskDone(login, givenTask){
    const todoList = getToDoListForUser({login})
    //On met à jour le status dans la liste, on ne supprime pas la tâche terminée.
    const newTasks = todoList.tasks.map(task => {
        if (task.id === givenTask.id)
            task.status="done"
        return  task
    })
    const newTodoList = {...todoList, tasks:newTasks} //Ajout de la nouvelle tache
    taskList = taskList.map(list => list.login === login ? newTodoList : list)
    return Promise.resolve(taskList)    
}
